# Run Database for the BPC-BDS course

Clone the repository: 

and run the following command in the root directory.
```shell
$ docker-compose up
```

### Connection details

**http://localhost/pgadmin4/**

johndoe@email.cz:postgres


### DB

**Host**: sedaq-db-postgres

**Port**: 5432

**Maintenance database**:  postgres

**Password**: postgres