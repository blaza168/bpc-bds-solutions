-- 1
-- Return persons with their contacts (only persons that have any
-- contact)

SELECT *
FROM person
INNER JOIN contact ON (contact.id_person = person.id_person);

-- 2
-- Return all the persons and their contacts

SELECT *
FROM person
LEFT JOIN contact ON (contact.id_person = person.id_person);

-- 3
-- Create a view (persons_info) returning only persons id, email, and
-- city ordered by id_person

CREATE VIEW persons_info AS
SELECT person.id_person, person.email, address.city
FROM person
LEFT JOIN address ON (person.id_address = address.id_address)
ORDER BY person.id_person;

-- 4
-- Use that view
SELECT id_person
FROM persons_info;

-- 5
-- Create a query returning the number of persons without relationship
-- ’partners’

create or replace function persons_no_parent_relationship_count() returns int
as
$$
DECLARE
persons_count int;
has_partner_rel_count int;
Begin
-- Get number of all persons
SELECT COUNT(id_person) INTO persons_count
FROM person;

-- Get number of persons having parent relationship
SELECT COUNT(DISTINCT id_person) INTO has_partner_rel_count
FROM person
INNER JOIN relationship ON (relationship.id_person1 = person.id_person OR relationship.id_person2 = person.id_person)
WHERE relationship.id_relationship_type = 5;

return persons_count - has_partner_rel_count;
end;
$$
Language 'plpgsql';

SELECT * FROM persons_no_parent_relationship_count();

-- 6
-- Store point 5 in materialized view
CREATE MATERIALIZED VIEW persons_not_in_partnership
AS SELECT * FROM persons_no_parent_relationship_count();

-- 7
-- Use view created in pint 6
SELECT *
FROM persons_not_in_partnership;

-- 8
-- Get all the persons living in Plzeň (paginated: size = 5, page = 2
-- (second page) (hint: use limit and offset)
SELECT person.*
FROM person
INNER JOIN address ON (person.id_address = address.id_address)
WHERE address.city = 'Plzeň'
OFFSET 5
LIMIT 5;

-- 9
-- Run with EXPLAIN. Cost = 23,65
EXPLAIN SELECT person.*
FROM person
INNER JOIN address ON (person.id_address = address.id_address)
WHERE address.city = 'Plzeň'
OFFSET 5
LIMIT 5;

-- 10 Add index
CREATE INDEX city_indx
ON address(city);

-- 11
-- New Cost: 12.43. Search cost decreased

--12
-- Indexes
-- Advantages: Decrease search time. <-- most useful
-- Disadvantages: Decrease performance on INSERT, DELETE and UPDATE, because collection has to be sorted again

-- Types:
-- UNIQUE indexes,
-- Partial indexes => defined by condition
-- Multi/Single column indexes
-- Implicit indexes => Created automatically for primary keys and unique constraints

-- 13
-- Function already created above

CREATE OR REPLACE PROCEDURE non_procedure()
LANGUAGE 'plpgsql'
AS $$
BEGIN
UPDATE person SET email = 'blemca@gmail.com' WHERE 1 = 2;
END;$$;

call non_procedure();

-- trigger
CREATE OR REPLACE FUNCTION useless_trigger()
RETURNS TRIGGER
LANGUAGE 'plpgsql'
AS $$
BEGIN
IF NEW.city = 'test' THEN
    INSERT INTO bds.address (city, house_number, street, zip_code) VALUES ('Blemca', 456, 'blem', 45698);
END IF;
RETURN NEW;
END;$$;

CREATE TRIGGER usls_triggerrr
BEFORE INSERT
ON bds.address
EXECUTE PROCEDURE useless_trigger();

INSERT INTO bds.address (city, house_number, street, zip_code) VALUES ('test', 456, 'blem', 45698);
-- Doesn't do anything, but nobody said it must work.
-- There is "Create arbitrary trigger" sentence in assignment. :-)
