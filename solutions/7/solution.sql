-- 4
DROP ROLE IF EXISTS instructor;
CREATE ROLE instructor WITH NOSUPERUSER ENCRYPTED PASSWORD 'batman';

-- 5
SELECT rolname FROM pg_roles;

-- 7
GRANT CONNECT ON DATABASE "db-training" TO instructor;
ALTER ROLE "instructor" WITH LOGIN;

-- 8
GRANT USAGE ON SCHEMA bds TO instructor;

-- 9
GRANT SELECT ON bds.person TO instructor;

-- 11
GRANT SELECT ON ALL TABLES IN SCHEMA bds TO instructor;

--db-training=> SELECT * FROM bds.address ORDER BY id_address LIMIT 3;
-- id_address |   city    | house_number |     street     | zip_code
--------------+-----------+--------------+----------------+----------
--          1 | Holice    |          315 | Nad Splavem    | 53401
--          2 | Holice    |          325 | Nad Splavem    | 53401
--          3 | Pardubice |          972 | Emy Destinnové | 53201
--(3 rows)

-- 12
REVOKE SELECT ON bds.person FROM instructor;

-- 13

CREATE VIEW bds.persons_view AS
SELECT person.id_person, person.email, person.id_address
FROM bds.person;

-- 14
GRANT SELECT ON bds.persons_view TO instructor;

-- 17
DROP OWNED BY instructor;
DROP ROLE instructor;
