
-- 1
SELECT * FROM person WHERE surname LIKE '%K';
-- 2
SELECT * FROM meeting ORDER BY start_time ASC LIMIT 1;
-- 3
SELECT * FROM person_role JOIN person ON (person.id_person = person_role.id_person) WHERE person_role.role IN ('CEO', 'ADMIN', 'CTO');
-- 4
UPDATE person SET surname = 'Nováková' WHERE id_person = 29;
-- 5
SELECT COUNT (title) FROM relationship_type;
-- 6
SELECT * FROM person JOIN contact ON (person.id_person = contact.id_person) JOIN address ON (person.id_address = address.id_address);
-- 7
SELECT city, COUNT (*) FROM address GROUP BY city;
-- 8
SELECT DISTINCT person.* FROM relationship JOIN person ON (relationship.id_person1 = person.id_person OR relationship.id_person2 = person.id_person) WHERE id_relationship_type = (SELECT id_relationship_type FROM relationship_type WHERE title = 'family');
-- 9
SELECT id_person FROM person WHERE age > 60 UNION SELECT id_person FROM person WHERE age < 18;
-- 10
SELECT COUNT(DISTINCT city) FROM address WHERE city IN (SELECT city FROM address GROUP BY city HAVING COUNT (*) > 0);
-- 11
SELECT * FROM person LEFT JOIN contact ON (person.id_person = contact.id_person) LEFT JOIN address ON (person.id_address = address.id_address) WHERE address.city = 'Brno';
-- 12
SELECT DISTINCT person.* FROM relationship JOIN person ON (relationship.id_person1 = person.id_person OR relationship.id_person2 = person.id_person) WHERE id_relationship_type = (SELECT id_relationship_type FROM relationship_type WHERE title = 'family');
-- 13
SELECT DISTINCT * FROM person RIGHT JOIN relationship ON (relationship.id_person1 = person.id_person OR relationship.id_person2 = person.id_person);
